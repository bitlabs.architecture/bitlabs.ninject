﻿namespace bitlabs.ninject
{
    using System;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true )]
    public class BootstrapCategoryAttribute: Attribute
    {

        #region Public Properties

        public string Category
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public BootstrapCategoryAttribute(string category)
        {
            this.Category = category;
        }

        public BootstrapCategoryAttribute()
            : this("*")
        {
        }

        #endregion

    }
}
