﻿namespace bitlabs.ninject
{
    using Ninject.Modules;
    using System.Collections.Generic;

    public interface INinjectModuleBootstrapper
    {

        IList<INinjectModule> GetModules();

    }
}
