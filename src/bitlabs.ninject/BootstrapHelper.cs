﻿namespace bitlabs.ninject
{
    using Ninject;
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    public static class BootstrapHelper
    {

        #region Private Methods

        private static string[] GetDllsToFindModules()
        {
            string directory = AppDomain.CurrentDomain.BaseDirectory;

            if (string.IsNullOrEmpty(directory))
                throw new ArgumentNullException(nameof(directory));

            return Directory.GetFiles(directory, "*.dll").Where(
                x => !Path.GetFileName(x).StartsWith("System.")
            ).ToArray();
        }

        private static bool IsOnCategory(Type type, string category)
        {
            if (category.Equals("*"))
                return true;

            return type.GetCustomAttributes<BootstrapCategoryAttribute>()
                        .Where(x => x.Category.Equals(category)).Count() > 0;
        }

        #endregion

        #region Public Methods

        public static IKernel LoadNinjectKernel()
            => BootstrapHelper.LoadNinjectKernel("*");

        public static IKernel LoadNinjectKernel(string category)
        {
            IKernel standardKernel = new StandardKernel();

            foreach (string assemblyFile in BootstrapHelper.GetDllsToFindModules())
            {
                Assembly assembly = Assembly.LoadFile(assemblyFile);

                foreach (Type type in assembly.GetTypes())
                {
                    Type[] interfaces = type.GetInterfaces();

                    if (interfaces.Contains(typeof(INinjectModuleBootstrapper)) 
                        && BootstrapHelper.IsOnCategory(type, category))
                    {
                        INinjectModuleBootstrapper bootstrap = (INinjectModuleBootstrapper)Activator.CreateInstance(type);
                        standardKernel.Load(bootstrap.GetModules());
                    }
                }
            }

            return standardKernel;
        }

        #endregion

    }
}
