﻿namespace bitlabs.ninject.tests
{
    using bitlabs.ninject.example.data;
    using bitlabs.ninject.example.services;
    using Ninject;
    using NUnit.Framework;

    [TestFixture]
    public class BootstrapHelperTests
    {
        #region Private Properties

        private IKernel _kernel
        {
            get;
            set;
        }

        #endregion

        #region NUnit Methods

        [SetUp]
        public void SetUp()
        {
            this._kernel = null;
        }

        #endregion

        #region Test Methods

        [Test]
        public void LoadNinjectKernel_LoadAllBootstrappers_ReturnKernel()
        {
            // Act 
            this._kernel = BootstrapHelper.LoadNinjectKernel();

            // Assert
            Assert.That(this._kernel.CanResolve<IDataContext>(), Is.True);
            Assert.That(this._kernel.CanResolve<IUsersService>(), Is.True);

            var dataContext = this._kernel.Get<IDataContext>();
            var userService = this._kernel.Get<IUsersService>();

            Assert.That(dataContext, Is.Not.Null);
            Assert.That(dataContext.TestMethod(), Is.True);
            Assert.That(userService, Is.Not.Null);
            Assert.That(userService.TestMethod(), Is.True);
        }

        [Test]
        public void LoadNinjectKernel_LoadDataCategory_ReturnKernel()
        {
            // Act 
            this._kernel = BootstrapHelper.LoadNinjectKernel("DataCategory");

            // Assert
            Assert.That(this._kernel.CanResolve<IDataContext>(), Is.True);
            Assert.That(this._kernel.CanResolve<IUsersService>(), Is.False);

            var dataContext = this._kernel.TryGet<IDataContext>();
            var userService = this._kernel.TryGet<IUsersService>();

            Assert.That(dataContext, Is.Not.Null);
            Assert.That(dataContext.TestMethod(), Is.True);
            Assert.That(userService, Is.Null);
        }

        [Test]
        public void LoadNinjectKernel_LoadServicesCategory_ReturnKernel()
        {
            // Act 
            this._kernel = BootstrapHelper.LoadNinjectKernel("ServicesCategory");

            // Assert
            Assert.That(this._kernel.CanResolve<IDataContext>(), Is.False);
            Assert.That(this._kernel.CanResolve<IUsersService>(), Is.True);

            var dataContext = this._kernel.TryGet<IDataContext>();
            var userService = this._kernel.TryGet<IUsersService>();

            Assert.That(dataContext, Is.Null);
            Assert.That(userService, Is.Not.Null);
            Assert.That(userService.TestMethod(), Is.True);
        }

        [Test]
        public void LoadNinjectKernel_LoadGlobalCategory_ReturnKernel()
        {
            // Act 
            this._kernel = BootstrapHelper.LoadNinjectKernel("GlobalCategory");

            // Assert
            Assert.That(this._kernel.CanResolve<IDataContext>(), Is.True);
            Assert.That(this._kernel.CanResolve<IUsersService>(), Is.True);

            var dataContext = this._kernel.Get<IDataContext>();
            var userService = this._kernel.Get<IUsersService>();

            Assert.That(dataContext, Is.Not.Null);
            Assert.That(dataContext.TestMethod(), Is.True);
            Assert.That(userService, Is.Not.Null);
            Assert.That(userService.TestMethod(), Is.True);
        }

        #endregion

    }
}
