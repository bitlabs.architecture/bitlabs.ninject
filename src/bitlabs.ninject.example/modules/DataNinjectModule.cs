﻿namespace bitlabs.ninject.example.modules
{
    using bitlabs.ninject.example.data;
    using Ninject.Modules;

    public class DataNinjectModule : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IDataContext>().To<DataContext>();
        }

    }
}
