﻿namespace bitlabs.ninject.example.modules
{
    using bitlabs.ninject.example.services;
    using Ninject.Modules;

    public class ServicesNinjectModule : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IUsersService>().To<UsersService>();
        }

    }
}
