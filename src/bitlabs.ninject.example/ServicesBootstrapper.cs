﻿namespace bitlabs.ninject.example
{
    using System.Collections.Generic;
    using bitlabs.ninject.example.modules;
    using Ninject.Modules;

    [BootstrapCategory("GlobalCategory")]
    [BootstrapCategory("ServicesCategory")]
    public class ServicesBootstrapper : INinjectModuleBootstrapper
    {

        #region Public Methods

        public IList<INinjectModule> GetModules()
        {
            return new List<INinjectModule>()
            {
                new ServicesNinjectModule(),
            };
        }

        #endregion

    }
}
