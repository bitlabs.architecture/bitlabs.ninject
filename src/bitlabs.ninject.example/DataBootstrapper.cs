﻿namespace bitlabs.ninject.example
{
    using System.Collections.Generic;
    using bitlabs.ninject.example.modules;
    using Ninject.Modules;

    [BootstrapCategory("GlobalCategory")]
    [BootstrapCategory("DataCategory")]
    public class DataBootstrapper : INinjectModuleBootstrapper
    {
        
        #region Public Methods

        public IList<INinjectModule> GetModules()
        {
            return new List<INinjectModule>()
            {
                new DataNinjectModule(),
            };
        }

        #endregion
    }
}
